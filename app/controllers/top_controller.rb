class TopController < ApplicationController
  def main
    if session[:login_uid]
      render  'main'
    else
      render  'login'
    end
  end
  def login
    if user = User.find_by_uid(params[:uid])
      if params[:uid] == user.uid and params[:pass] == user.pass
        session[:login_uid] = params[:uid]
        redirect_to '/'
      else
        render 'error'
      end
    else
      render 'error'
    end
  end
  def logout
    session.delete(:login_uid)
    redirect_to '/'
  end
end
